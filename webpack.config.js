const webpackConfig = require("webpack");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const PathPlugin = require("path");
const { merge } = require("webpack-merge");

function path(path) {
  return PathPlugin.join(__dirname, "src", path);
}

module.exports = (env) => {
  const production = env.MODE === "prod";
  const port = env.PORT;

  const configuration = production
    ? {
        mode: "production",
        performance: {
          hints: false,
        },
      }
    : {
        mode: "development",
        devtool: "eval-cheap-module-source-map",
        devServer: {
          historyApiFallback: true,
          port,
        },
      };

  return merge(configuration, {
    entry: [path("main/Main")],
    resolve: {
      alias: {
        process: "process/browser",
      },
      extensions: [".ts", ".tsx", ".js"],
      fallback: {
        crypto: require.resolve("crypto-browserify"),
        stream: require.resolve("stream-browserify"),
        assert: require.resolve("assert"),
      },
    },
    output: {
      filename: "[name].[chunkhash].js",
      path: PathPlugin.join(__dirname, "target"),
      publicPath: "/",
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)$/,
          include: path("main"),
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/typescript",
              ["@babel/react", { runtime: "automatic" }],
            ],
          },
        },
        {
          test: /\.(png|svg|ico|xml|webmanifest)$/,
          include: path("resources"),
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[name].[ext]",
                context: "resources",
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new ForkTsCheckerWebpackPlugin({
        typescript: {
          configOverwrite: {
            compilerOptions: {
              noUnusedLocals: production,
              noUnusedParameters: production,
            },
          },
        },
      }),
      new HtmlWebpackPlugin({ template: path("main/index.html") }),
      new webpackConfig.ProvidePlugin({ Buffer: ["buffer", "Buffer"], process: "process/browser" }),
    ].filter(Boolean),
  });
};
