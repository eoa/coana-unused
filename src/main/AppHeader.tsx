import { AppBar, Grid, Toolbar, Typography } from "@mui/material";
import { Buffer } from "buffer";

export function AppHeader(): JSX.Element {
  const buf = Buffer.from([1, 2, 3, 4]);
  console.log(buf);

  return (
    <AppBar>
      <Toolbar>
        <Grid container alignItems={"center"} justifyContent={"space-between"} spacing={4}>
          <Grid item>
            <Typography>Coana</Typography>
          </Grid>
          <Grid item>
            <Typography>Unused</Typography>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}
