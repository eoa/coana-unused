import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { styled } from "@mui/system";

export function AppContent(): JSX.Element {
  const StyledTextField = styled(TextField, {
    name: "StyledTextField",
  })({
    "& .MuiOutlinedInput-root:hover": {
      "& > fieldset": {
        borderColor: "gold",
      },
    },
  });

  return (
    <Box margin={4}>
      <Card>
        <CardHeader title={"Card Header"} />
        <CardContent>
          <Typography>Lorem Ipsum</Typography>
          <Grid container width={600}>
            <Grid item xs={6}>
              <StyledTextField variant="outlined" label="styled 1" />
              <StyledTextField variant="outlined" label="styled 2" />
              <StyledTextField variant="outlined" label="styled 3" />
              <StyledTextField variant="outlined" label="styled 4" />
            </Grid>
            <Grid item xs={6}>
              <TextField variant="outlined" label="normal 1" />
              <TextField variant="outlined" label="normal 2" />
              <TextField variant="outlined" label="normal 3" />
              <TextField variant="outlined" label="normal 4" />
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button variant={"contained"} color={"primary"}>
            I do nothing
          </Button>
        </CardActions>
      </Card>
    </Box>
  );
}
