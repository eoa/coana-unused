import {createRoot} from "react-dom/client";
import {BrowserRouter} from "react-router-dom";
import {AppContent} from "./AppContent";
import {AppHeader} from "./AppHeader";
import {WithTheme} from "./Theme";

const container = document.getElementById("root");
const root = createRoot(container!);
root.render(
    <BrowserRouter>
      <WithTheme>
        <AppHeader/>
        <AppContent/>
      </WithTheme>
    </BrowserRouter>,
);
