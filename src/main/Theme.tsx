import {
  createTheme,
  SimplePaletteColorOptions,
  StyledEngineProvider,
  ThemeProvider,
} from "@mui/material";
import React, { PropsWithChildren } from "react";

declare module "@mui/material/styles" {
  interface TypographyVariants {
    highlightedNumber1: React.CSSProperties;
    highlightedNumber2: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    highlightedNumber1?: React.CSSProperties;
    highlightedNumber2?: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    highlightedNumber1: true;
    highlightedNumber2: true;
  }
}

interface SimpleTypeAction {
  disabled: string;
  disabledBackground: string;
}

interface SimplePalette {
  primary: SimplePaletteColorOptions;
  secondary: SimplePaletteColorOptions;
  action: SimpleTypeAction;
}

// const PARTISIA_PALETTE = {
//   primary: {
//     main: "#000045",
//     contrastText: "#FFFFFF",
//   },
//   secondary: {
//     main: "#120CE5",
//     contrastText: "#FFFFFF",
//   },
//   error: {
//     main: "#c62828",
//   },
//   warning: {
//     main: "#A65D00",
//   },
//   info: {
//     main: "#9c27b0",
//   },
//   success: {
//     main: "#008700",
//   },
//   text: {
//     primary: "#333333",
//   },
//   background: {
//     default: "#FFFFFF",
//     tableRow: "#333333",
//   },
//   action: {
//     disabled: "rgba(0, 0, 0, 0.26)",
//     disabledBackground: "rgba(0, 0, 0, 0.12)",
//   },
//   buttonBackground: (_: SimplePalette) => ({}),
//   highlightedNumberColor: (p: SimplePalette) => ({
//     background: `linear-gradient(90deg, ${p.primary.main} 0%, ${p.secondary.main} 100%)`,
//     backgroundClip: "text",
//     color: "transparent",
//   }),
// };

const PARTISIA_BLOCKCHAIN_PALETTE = {
  primary: {
    main: "#0A0028",
    contrastText: "#FFFFFF",
  },
  secondary: {
    main: "#269372",
    light: "#26d495",
    contrastText: "#FFFFFF",
  },
  error: {
    main: "#c62828",
  },
  warning: {
    main: "#A65D00",
  },
  info: {
    main: "#9c27b0",
  },
  success: {
    main: "#008700",
  },
  text: {
    primary: "#0A0028",
  },
  background: {
    default: "#FFFFFF",
  },
  action: {
    disabled: "rgba(0, 0, 0, 0.26)",
    disabledBackground: "rgba(0, 0, 0, 0.12)",
  },
  buttonBackground: (p: SimplePalette) => ({
    background: `linear-gradient(90deg, ${p.secondary.main} 0%, ${p.secondary.light} 100%)`,
    "&:disabled": {
      background: "none",
      color: p.action.disabled,
      backgroundColor: p.action.disabledBackground,
    },
  }),
  highlightedNumberColor: (p: SimplePalette) => ({ color: p.secondary.main }),
};

const palette = PARTISIA_BLOCKCHAIN_PALETTE;

export const DRAWER_WIDTH = "327px";
export const HEADER_HEIGHT = "80px";

const theme = createTheme({
  palette,
  spacing: 8,
  typography: {
    fontFamily: "ubuntu",
    fontSize: 12,
    fontWeightRegular: 400,
    fontWeightBold: 500,
    h1: undefined,
    h2: undefined,
    h3: undefined,
    h4: undefined,
    h5: {
      fontFamily: "Playfair Display",
      fontWeight: 400,
      fontSize: 24,
      lineHeight: "32px",
    },
    h6: {
      fontFamily: "Playfair Display",
      fontWeight: 500,
      fontSize: 20,
      lineHeight: "27px",
      letterSpacing: 0.15,
    },
    subtitle1: {
      fontFamily: "Playfair Display",
      fontWeight: 500,
      fontSize: 16,
      letterSpacing: 0.15,
    },
    subtitle2: undefined,
    body1: {
      fontSize: 16,
      letterSpacing: 0.5,
    },
    body2: {
      fontSize: 14,
      lineHeight: "18px",
      letterSpacing: 0.25,
    },
    caption: {
      fontSize: 12,
      lineHeight: "18px",
    },
    highlightedNumber1: {
      fontFamily: "ubuntu",
      fontWeight: 500,
      fontSize: 34,
      letterSpacing: "0.25px",
      lineHeight: "39px",
      ...palette.highlightedNumberColor(palette),
    },
    highlightedNumber2: {
      fontFamily: "ubuntu",
      fontWeight: 500,
      fontSize: 20,
      letterSpacing: "0.15px",
      lineHeight: "23px",
      ...palette.highlightedNumberColor(palette),
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        root: {
          position: "sticky",
          backgroundColor: palette.primary.main,
          boxShadow: "2px 0px 8px rgba(17, 17, 17, 0.1)",
        },
      },
    },
    MuiToolbar: {
      styleOverrides: {
        root: {
          height: HEADER_HEIGHT,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          display: "block",
          marginTop: 4,
          borderRadius: 4,
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          letterSpacing: 1.25,
        },
        contained: {
          fontWeight: 500,
          fontSize: "14px",
          letterSpacing: 1.25,
        },
        containedSecondary: {
          ...palette.buttonBackground(palette),
        },
      },
    },
    MuiDivider: {
      styleOverrides: {
        root: {
          borderColor: palette.text.primary + "1A",
        },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          color: palette.text.primary,
          backgroundColor: palette.background.default,
        },
      },
    },
    MuiTable: {
      styleOverrides: {
        root: {
          tableLayout: "fixed",
          textOverflow: "ellipsis",
        },
      },
    },
    MuiTableFooter: {
      styleOverrides: {
        root: {
          height: "56px",
        },
      },
    },
    MuiTableRow: {
      styleOverrides: {
        root: ({ ownerState }) => {
          const isBodyRow = ownerState.head !== true && ownerState.footer !== true;
          return {
            "&:nth-of-type(odd)": {
              backgroundColor: isBodyRow ? palette.text.primary + "08" : "transparent",
            },
            "&:nth-of-type(even)": {
              backgroundColor: isBodyRow ? palette.text.primary + "0D" : "transparent",
            },
          };
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          whiteSpace: "nowrap",
        },
        head: {
          fontWeight: 500,
          fontSize: "14px",
          color: palette.text.primary,
          borderBottom: "none",
          paddingBottom: 8,
        },
        body: {
          fontSize: "16px",
          fontWeight: "inherit",
          borderBottom: "1pt solid white",
          overflow: "hidden",
          textOverflow: "ellipsis",
        },
      },
    },
    MuiDialogTitle: {
      defaultProps: {
        color: "primary",
      },
      styleOverrides: {
        root: {
          fontFamily: "Playfair Display",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: "16px",
          lineHeight: "21px",
          letterSpacing: "0.15px",
        },
      },
    },
    MuiDialogContentText: {
      styleOverrides: {
        root: {
          color: palette.text.primary,
          paddingBottom: 12,
          fontStyle: "normal",
          fontWeight: 400,
          fontSize: 14,
          letterSpacing: 0.25,
        },
      },
    },
    MuiDialogActions: {
      styleOverrides: {
        root: {
          padding: 24,
          paddingTop: 0,
        },
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          backgroundColor: palette.background.default,
          borderRadius: 20,
          boxShadow: "0px 2px 20px rgba(17, 17, 17, 0.12)",
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: palette.secondary.main,
        },
      },
    },
    MuiDrawer: {
      styleOverrides: {
        root: {
          position: "fixed",
          backgroundColor: palette.background.default,
          width: DRAWER_WIDTH,
          boxShadow: "2px 0px 8px rgba(17, 17, 17, 0.1)",
        },
        paper: {
          width: DRAWER_WIDTH,
          boxSizing: "border-box",
        },
      },
    },
    MuiListItemButton: {
      styleOverrides: {
        root: {
          borderRadius: 6,
          // A color propagation bug prevents color to icon with "selected" property on button, so
          // we set color to inherit, and propagate from 'Div' instead.
          color: "inherit",
          "&:hover": {
            backgroundColor: palette.text.primary + "0D",
            boxShadow: `0px 0px 8px ${palette.text.primary}03`,
          },
          "&.Mui-selected, &.Mui-selected:hover": {
            backgroundColor: palette.primary.main,
            color: palette.primary.contrastText,
            fontWeight: 500,
          },
        },
      },
    },
    MuiListItemIcon: {
      styleOverrides: {
        root: {
          color: "inherit",
        },
      },
    },
    MuiListItemText: {
      styleOverrides: {
        primary: {
          fontSize: 20,
          lineHeight: "23px",
          letterSpacing: 0.15,
          fontWeight: "inherit",
        },
      },
    },
    MuiMenu: {
      styleOverrides: {
        root: {
          "& .MuiPaper-root": { borderRadius: 0 },
        },
      },
    },
  },
});

export function WithTheme({ children }: PropsWithChildren<Record<never, never>>): JSX.Element {
  return (
    <ThemeProvider theme={theme}>
      <StyledEngineProvider injectFirst>{children}</StyledEngineProvider>
    </ThemeProvider>
  );
}
